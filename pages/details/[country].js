import React , { useState } from 'react'
import Head from 'next/head';
import { useEffect} from 'react';


const CountryData = ({ data }) => {
    let curr = "NULL";
    let nat = "NULL" ;
    let native = "NULL";
    let lang = "NULL";
    let language = "NULL";
    if(data[0].currencies){
        curr = Object.values(data[0].currencies)[0].name;
    }

    if(data[0].name.nativeName){
        nat = Object.values(data[0].name.nativeName)[0];
        native = Object.values(nat);
    }

    if(data[0].languages)
    {
        lang = Object.values(data[0].languages)[0];
        language = Object.values(lang);
    }
    const [myData , setMyData] = useState([])

    const tempFun = async function(){
        let result = [];
        if(data[0].borders){
            await Promise.all(
                data[0].borders.map(async (ev) => {
                    const result1 = await fetch('https://restcountries.com/v3.1/alpha/' + ev);
                    const data1 = await result1.json();

                    result.push(data1[0]);
                })
            )
        }

        setMyData(result)
    }

    useEffect(() => {
        tempFun()
    } , [])


    return (
      <div>
        <Head>
            <title></title>
        </Head>
        <div className='container mt-5'>
            <div className='row justify-content-center'>
                <div className='col-sm-8 border'>
                    <h1>{data[0].name.common}</h1>
                    <div className='row'>
                        <div className='col-sm-5 mt-2'>
                            <img src={data[0].flags.png} width = "100%" height = "60%" />
                        </div>
                        <div className='col-sm mt-1'>
                            <p><b>Native Name :</b>{native}</p>
                            <p><b>Capital :</b> {data[0].capital && data[0].capital[0]}</p>
                            <p><b>Population :</b> {data[0].population}</p>
                            <p><b>Region :</b> {data[0].region}</p>
                            <p><b>Sub-region :</b> {data[0].subregion}</p>
                            <p><b>Area :</b> {data[0].area}</p>
                            <p><b>Country Code :</b> {data[0].region}</p>
                            <p><b>Languages :</b> {language}</p>
                            <p><b>Currencies :</b> {curr}</p>
                            <p><b>Timezones :</b> {data[0].timezones && data[0].timezones[0]}</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div className='container mt-5'>
            <div className='row justify-content-center'>
                <div className='col-sm-8 border'>
                    <h2>Neighbouring Countries</h2>
                    {
                        myData.map((ev) => {
                            return <img src={ev.flags.png} width="200px" height="150px" style={{'margin' : '20px'}} key = {ev.cca3}/>
                        })
                    }
                </div>
            </div>
        </div>
      </div>  
      )
  }
  
  export default CountryData;

export async function getStaticPaths(){
    const url = "https://restcountries.com/v3.1/all"
    const result = await fetch(url);
    const data = await result.json();

    const paths = data.map((info) => {
        return {
            params: {
                country: info.cca3.toString(),
            }
        }
    });
    return {
        paths,
        fallback: false,
    }
}

export async function getStaticProps(context){
    const cca3 = context.params.country;
    const result = await fetch("https://restcountries.com/v3.1/alpha/" + cca3);
    const data = await result.json();
    
  
    return {
      props: {
        data,
      }
    };
  }


